var app = angular.module('juicy', ["ngRoute",'uiGmapgoogle-maps','angular-loading-bar']);
app.controller('homeController', function($scope,$http) {
	
	$scope.nameFilter = null;
	$scope.searchFilter = function (user) {
		var re = new RegExp($scope.nameFilter, 'i');
		return !$scope.nameFilter || re.test(user.name) || re.test(user.email);
	};
	
	$http.get("http://jsonplaceholder.typicode.com/users").then(function (response) {
		$scope.data = response.data;
	});
});

app.controller('bController', function($scope,$http,$routeParams) {
	$http.get("http://jsonplaceholder.typicode.com/users/"+$routeParams.id).then(function (response) {
		$scope.single = response.data;
	});
});

app.controller('categoriesController', function($scope,$http,$routeParams) {
	
	$http.get("/categorise-user/index").then(function (response) {
		$scope.categories = response.data.user;
	});
});

app.controller('mapsController', function($scope,$http,$routeParams) {

	$scope.map = {center: {latitude: 40.1451, longitude: -99.6680 }, zoom: 10 };
    $scope.options = {scrollwheel: false};
    $scope.coordsUpdates = 0;
    $scope.dynamicMoveCtr = 0;
	$scope.marker = [];
	var id =1;
    $http.get("/categorise-user/maps").then(function (response) {

		$scope.coordinates = response.data.coordinates;
        $scope.coordinates.forEach(function(co) {
            $scope.map = {center: {latitude: co.lat, longitude: co.lon }, zoom: 2 };
	        mark = {
			id: id,
            coords: {
                latitude: co.lat,
                longitude: co.lon
            }
        };
		$scope.marker.push(mark);
		id++;
        });
    });
});


app.config(['$routeProvider', function($routeProvider) {
	$routeProvider.
		when("/bcards/:id", {templateUrl: "/angular-templates/BusinessCards.ss", controller: "bController"}).
		when("/categorise", {templateUrl: "/angular-templates/CategoriseUsers.ss", controller: "categoriesController"}).
		when("/map", {templateUrl: "/angular-templates/Maps.ss", controller: "mapsController"}).
		when("/", {templateUrl: "/angular-templates/ShowUsers.ss", controller: "homeController"}).
		otherwise({redirectTo: '/'});
}],['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false; 
    cfpLoadingBarProvider.includeBar = true; 
}]);

