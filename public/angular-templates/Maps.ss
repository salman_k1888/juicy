<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 float-right">

        <div id="map_canvas" style="height:500px; !important">
            <ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options">
                <ui-gmap-marker ng-repeat="mk in marker" coords="mk.coords" idkey="mk.id">
                </ui-gmap-marker>
            </ui-gmap-google-map>
        </div>
    </div>
</div>
