<a href="./#/">Back to listing</a>

<div class="container">
    <div class="card">
        <div class="card-body front side">
            <h3 class="">{{ single.name }}</h3>
            <div class="hr"></div>
        </div>
        <div class="back side">
            <div class="card-body">
                <h2 class="font-weight-bold">{{ single.company.name }}</h2>
                <h6>{{ single.website }}</h6>
            </div>
            <div class="card-footer">
                <span class="float-left">E: {{ single.email }}</span><span class="float-right">P: {{ single.phone }}</span>
            </div>
        </div>
    </div>
</div>

