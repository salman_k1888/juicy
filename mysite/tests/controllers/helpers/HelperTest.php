<?php
use SilverStripe\Dev\SapphireTest;
use SilverStripe\Helpers\Helper;
use SilverStripe\ORM\DataObject;


class HelperTest extends SapphireTest
{
    private $user;
    private $res;
    public function testMyMethod()
    {
        $this->assertEquals(2, Helper::MyMethod());
    }
    public function testgetCategorisedUsers()
    {
        
        $this->user = (object) [
            0 => (object)["Name"=>"Sam"],
            1 => (object)["Name"=>"James"],
            2 => (object)["Name"=>"John"]
        ];
        fwrite(STDERR, print_r($this->user, TRUE));
      
        $this->res =  array(
            'J'   => array(0=>'James',1=>'John'),
            'S' => array(0=>"Sam"),
        );

        $this->assertEquals($this->res, Helper::getCategorisedUsers($this->user));
    }
}
?>