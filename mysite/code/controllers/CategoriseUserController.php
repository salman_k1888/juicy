<?php
use SilverStripe\Control\Controller;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\ORM\DataObject;
use SilverStripe\Helpers\Helper;
use SilverStripe\ORM\ArrayList;
use SilverStripe\view\ArrayData;
use SilverStripe\view\JSONDataFormatter;


class CategoriseUserController extends Controller
{
    private static $allowed_actions = [
        'maps',
        'index',
        'jUser'
    ];

    public function index(HTTPRequest $request) 
    {
        $users = User::get();
        
		$result =Helper::getCategorisedUsers($users);
        return json_encode(array('user'=>$result));    
    }

    public function maps(HTTPRequest $request) 
    {
        $users = User::get()->setQueriedColumns(['Name', 'ZIP']);
        $result =Helper::getCoordinates($users);
        
        return json_encode(array('coordinates'=>$result));    
    }
    	
}
?>