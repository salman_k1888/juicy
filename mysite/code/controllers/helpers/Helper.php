<?php
namespace SilverStripe\Helpers;

class Helper {

	public static function getCategorisedUsers( $obj )
	{ 
		$groups = array();

		foreach(range('A', 'Z') as $char) {
			foreach($obj as $user) {
				if(strtoupper($user->Name[0]) == $char){
					$groups[$char][] = $user->Name;
				}
			}
		}
		return $groups;
	}
	public static function getCoordinates( $obj ){

		$array = array();
        foreach($obj as $key  => $u)
        {
               $array[$key]['lon'] = $u->Longitude; 
               $array[$key]['lat'] = $u->Latitude; 
		}
		return $array;
	}

}