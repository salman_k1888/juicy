<?php

use SilverStripe\Dev\BuildTask;
class ImportContactsTask extends BuildTask 
{
    protected $title            = 'Contacts';
    protected $description      = "Covert users data into objects"; 
    protected $apiUrl           = 'http://jsonplaceholder.typicode.com/users';

    /**
	 * @param SS_HTTPRequest $request
	 */
	public function run($request) {
        
        echo "Running dev/build <br /> ";

        $client         =   new GuzzleHttp\Client();
        $res            =   $client->request('GET',$this->apiUrl , [
            'auth' => ['', '']
        ]);

        try {
			$results = json_decode($res->getBody(), false);
		} catch(Exception $e) {
			exit('Bad Output');
        }
        User::get()->removeAll();
        foreach($results as $rs) {
            echo "Insert data for ".$rs->name." <br /> ";

            $user           = User::create();
            $user->Name     = $rs->name;
            $user->UserName = $rs->username;
            $user->Email    = $rs->email;
            $user->Address  = $rs->address->suite ." - ". $rs->address->street. " - ".$rs->address->city." - ".$rs->address->zipcode;
            $user->Phone    = $rs->phone;
            $user->Website  = $rs->website;
            $user->Company  = $rs->company->name;
            $user->Latitude = $rs->address->geo->lat;
            $user->Longitude= $rs->address->geo->lng;
            $user->write();
        }
        echo "Done";
    }
}
?>