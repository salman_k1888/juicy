<?php

use SilverStripe\ORM\DataObject;

class User extends DataObject 
{
    private static $db = [
        'Name' => 'Varchar(25)',
        'UserName' => 'Varchar(25)',
        'Email' => 'Varchar(60)',
        'Address' => 'Text',
        'Phone' => 'Varchar(50)',
        'Website' => 'Varchar(25)',
        'Company' => 'Text',
        'Latitude' => 'Varchar(40)',
        'Longitude' => 'Varchar(40)',        

    ];
}

?>